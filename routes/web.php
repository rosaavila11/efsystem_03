<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');


});




//login
Route::post('login', ['as' =>'login', 'uses' => 'AuthController@postLogin']);

//Route::post('/login', ['as' =>'login', 'uses' => 'AuthController@postLogin']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
Route::get('/register', ['as' =>'register', 'uses' => 'AuthController@getRegister']);
// Registration routes...

Route::get('/home', ['middleware' => 'auth', 'as' => 'home', 'uses' => 'HomeController@home']);


////
// Registration routes...


// Usuarios routes...
Route::get('/gestionar-usuario/{page?}', 'ListadoController@listado_usuarios');

// Usuarios routes...
Route::get('/gestionar-usuario', 'UsuarioController@index');
Route::get('/crear-usu', 'UsuariosController@store1');

Route::get('/rol-usuario', 'UsuarioController@rol');
Route::get('/perfil-usuario', 'UsuarioController@perfil');
Route::get('/cargo-usuario', 'UsuarioController@cargo');

//Compras routes
Route::get('/orden-compra', 'ComprasController@index');
//Inventario routes
Route::get('/productos-terminados', 'InventarioController@productosTerminados');
Route::get('/productos-compuestos', 'InventarioController@productosCompuestos');
Route::get('/servicios', 'InventarioController@servicios');
Route::get('/unidades', 'InventarioController@unidades');
Route::get('/marcas', 'InventarioController@marcas');
Route::get('/operacion-precio', 'InventarioController@operacionPrecio');
Route::get('/departamentos', 'InventarioController@departamentos');
Route::get('/depositos', 'InventarioController@depositos');
Route::get('/moneda', 'InventarioController@moneda');



