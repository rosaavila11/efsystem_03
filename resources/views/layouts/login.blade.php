<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!DOCTYPE html>
	<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	
	<!-- Hojas de estilo para trabajar con Internet -->
	<!--<script src="https://use.fontawesome.com/765290634d.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="/app.css"> </head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">-->
	
	<!-- Hojas de estilo para trabajar Local -->
	<link rel="shortcut icon" href="/img/favicon.ico" />
	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css" type="text/css">   
	<link rel="stylesheet" href="/app.css"> 
	<link rel="stylesheet" type="text/css" href="/libs/DataTables/datatables.css">
	
    <title>Ef System</title>
	</head>
	<body>
		@yield('content')
	</body>