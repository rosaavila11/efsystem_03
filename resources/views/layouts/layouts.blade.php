<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!DOCTYPE html>
	<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	
	<!--<script src="https://use.fontawesome.com/765290634d.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="/app.css"> </head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/libs/DataTables/datatables.css">-->
    <link rel="shortcut icon" href="/img/favicon.ico" />
	<title>Ef System</title>
	
	<!-- Hojas de estilo para trabajar Local -->
	<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css" type="text/css">   
	<link rel="stylesheet" href="/app.css"> 
	<link rel="stylesheet" type="text/css" href="/libs/DataTables/datatables.css">
	
</head>
	<body class="">
		<div id="dashboard-container-main" class="row">
			<header>
				<div class="header-container row">
					<span class="action_logo">
						<h1 class="app-title">
							<img class="app-logo" src="/img/ef-logo.png" alt="ef">
							<span>System</span>
						</h1>
					</span>
					<div class="user-actions">
						<div class="user-menu">
							<img src="/img/user-icon.png" alt="imagen que representa al usuario, dentro del sistema">
							<span>Administrador</span>
							<i class="fa fa-chevron-down"></i>
						</div>
					</div>
				</div>
			</header>
			<div class="sidebar-container short">
				<div class="sidebar">
					<nav>
						<ul>
							<li class="sidebar-item">
								<a title="Usuarios">
									<i class="fa fa-id-card-o" aria-hidden="true"></i>
									<span>
										<span class="item-title">Usuarios</span>
										<i class="chevron fa fa-chevron-down" aria-hidden="true"></i>
									</span>
								</a>
								<ul class="sidebar-submenu">
									<li class="sidebar-item">
										<a href="{{url('register')}}" title="Crear">
											<span>
												<span class="item-title">Crear</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a href="{{url('gestionar-usuario')}}" title="Gestionar">
											<span>
												<span class="item-title">Gestionar</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a href="{{url('rol-usuario')}}" title="Usuarios">
											<span>
												<span class="item-title">Roles</span>
											</span>
										</a>
                                	</li>
									<li class="sidebar-item">
										<a href="{{url('perfil-usuario')}}" title="Usuarios">
											<span>
												<span class="item-title">Perfiles</span>
											</span>
										</a>
									</li>
									<!--<li class="sidebar-item">
										<a href="{{url('cargo-usuario')}}" title="Usuarios">
											<span>
												<span class="item-title">Cargos</span>
											</span>
										</a>
									</li>-->
								</ul>
							</li>
							<li class="sidebar-item">
								<a title="Inventario">
									<i class="fa fa-file-text-o" aria-hidden="true"></i>
									<span>
										<span class="item-title">Inventario</span>
										<i class="chevron fa fa-chevron-down" aria-hidden="true"></i>
									</span>
								</a>
								<ul class="sidebar-submenu">
									<li class="sidebar-item">
										<a href="{{url('productos-terminados')}}" title="Usuarios">
											<span>
												<span class="item-title">Productos terminados</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a href="{{url('productos-compuestos')}}" title="Usuarios">
											<span>
												<span class="item-title">Productos compuestos</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a href="{{url('servicios')}}" title="Usuarios">
											<span>
												<span class="item-title">Servicios</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a href="{{url('unidades')}}" title="Usuarios">
											<span>
												<span class="item-title">Unidades</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a  href="{{url('marcas')}}" title="Usuarios">
											<span>
												<span class="item-title">Marcas</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a  href="{{url('operacion-precio')}}" title="Usuarios">
											<span>
												<span class="item-title">Operaciones de precio</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a href="{{url('departamentos')}}" title="Usuarios">
											<span>
												<span class="item-title">Departamentos</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a href="{{url('depositos')}}" title="Usuarios">
											<span>
												<span class="item-title">Depositos</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a href="{{url('moneda')}}" title="Usuarios">
											<span>
												<span class="item-title">Monedas</span>
											</span>
										</a>
									</li>
								</ul>
							</li>
							<li class="sidebar-item">
								<a title="Compras">
									<i class="fa fa-shopping-bag" aria-hidden="true"></i>
									<span>
										<span class="item-title">Compras</span>
										<i class="chevron fa fa-chevron-down" aria-hidden="true"></i>
									</span>
								</a>
								<ul class="sidebar-submenu">
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Ordenes</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Devoluciones</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Recepcion de facturas</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Recepcion de ordenes</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Recepcion de notas</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Recepcion de factura</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Factura de gastos</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Retenciones</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Proveedores</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Area de administrador</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Reportes</span>
											</span>
										</a>
									</li>
								</ul>
							</li>
							<li class="sidebar-item">
								<a title="Cuentas por cobrar">
									<i class="fa fa fa-outdent" aria-hidden="true"></i>
									<span>
										<span class="item-title">Cuentas por cobrar</span>
										<i class="chevron fa fa-chevron-down" aria-hidden="true"></i>
									</span>
								</a>
								<ul class="sidebar-submenu">
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Perfiles</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Roles</span>
											</span>
										</a>
									</li>
								</ul>
							</li>
							<li class="sidebar-item">
								<a title="Cuentas por pagar">
									<i class="fa fa-indent" aria-hidden="true"></i>
									<span>
										<span class="item-title">Cuentas por pagar</span>
										<i class="chevron fa fa-chevron-down" aria-hidden="true"></i>
									</span>
								</a>
								<ul class="sidebar-submenu">
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Perfiles</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Roles</span>
											</span>
										</a>
									</li>
								</ul>
							</li>
							<li class="sidebar-item">
								<a title="Ventas">
									<i class="fa fa-shopping-basket" aria-hidden="true"></i>
									<span>
										<span class="item-title">Ventas</span>
										<i class="chevron fa fa-chevron-down" aria-hidden="true"></i>
									</span>
								</a>
								<ul class="sidebar-submenu">
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Perfiles</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Roles</span>
											</span>
										</a>
									</li>
								</ul>
							</li>
							<li class="sidebar-item">
								<a title="Seguridad">
									 <i class="fa fa-key" aria-hidden="true"></i>
									<span>
										<span class="item-title">Seguridad</span>
										<i class="chevron fa fa-chevron-down" aria-hidden="true"></i>
									</span>
								</a>
								<ul class="sidebar-submenu">
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Perfiles</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Roles</span>
											</span>
										</a>
									</li>
								</ul>
							</li>
							<li class="sidebar-item">
								<a title="Reportes">
									<i class="fa fa-file-text" aria-hidden="true"></i>
									<span>
										<span class="item-title">Reportes</span>
										<i class="chevron fa fa-chevron-down" aria-hidden="true"></i>
									</span>
								</a>
								<ul class="sidebar-submenu">
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Perfiles</span>
											</span>
										</a>
									</li>
									<li class="sidebar-item">
										<a title="Usuarios">
											<span>
												<span class="item-title">Roles</span>
											</span>
										</a>
									</li>
								</ul>
							</li>

						</ul>
					</nav>
				</div>
			</div>
			<div id="layout" style="width:100%">
				@yield('content')
			</div>
    	</div>
		@yield('scripts')
	</body>
</html>