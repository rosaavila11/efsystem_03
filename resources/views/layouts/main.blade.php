<!DOCTYPE html>
<!--suppress ALL -->
<html lang="es">
<head>
<title>SIGECA</title>
<!--@yield('includesHead')-->
	<link href="/app.css" rel="stylesheet">
</head>
<body>
	<div id="dashboard-container-main" class="row">
    <header>
        <div class="header-container row">
            <span class="action_logo">
                <h1 class="app-title">
                    <img class="app-logo" src="/img/ef-logo.png" alt="ef">
                    <span>System</span>
                </h1>
            </span>
            <div class="user-actions">
                <div class="user-menu">
                    <img src="/img/user-icon.png" alt="imagen que representa al usuario, dentro del sistema">
                    <span>Administrador</span>
                    <i class="fa fa-chevron-down"></i>
                </div>
            </div>
        </div>
    </header>
    <div class="sidebar-container short">
        <div class="sidebar">
            <nav>
                <ul>
                    <li class="sidebar-item">
                        <a title="Usuarios">
                            <i class="fa fa-id-card-o" aria-hidden="true"></i>
                            <span>
                                <span class="item-title">Usuarios</span>
                                <i class="chevron fa fa-chevron-down" aria-hidden="true"></i>
                            </span>
                        </a>
                        <ul class="sidebar-submenu">
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Perfiles</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Roles</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-item">
                        <a title="Inventario">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                            <span>
                                <span class="item-title">Inventario</span>
                                <i class="chevron fa fa-chevron-down" aria-hidden="true"></i>
                            </span>
                        </a>
                        <ul class="sidebar-submenu">
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Productos terminados</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Productos compuestos</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Servicios</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Unidades</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Marcas</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Operaciones de precio</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Departamentos</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Depositos</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Depositos</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Monedas</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-item">
                        <a title="Compras">
                            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                            <span>
                                <span class="item-title">Compras</span>
                                <i class="chevron fa fa-chevron-down" aria-hidden="true"></i>
                            </span>
                        </a>
                        <ul class="sidebar-submenu">
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Registros</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Ordenes</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Devoluciones</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Recepcion de facturas</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Recepcion de ordenes</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Recepcion de notas</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Recepcion de factura</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Factura de gastos</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Retenciones</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Proveedores</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Area de administrador</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Reportes</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-item">
                        <a title="Cuentas por cobrar">
                            <i class="fa fa fa-outdent" aria-hidden="true"></i>
                            <span>
                                <span class="item-title">Cuentas por cobrar</span>
                                <i class="chevron fa fa-chevron-down" aria-hidden="true"></i>
                            </span>
                        </a>
                        <ul class="sidebar-submenu">
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Perfiles</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Roles</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-item">
                        <a title="Cuentas por pagar">
                            <i class="fa fa-indent" aria-hidden="true"></i>
                            <span>
                                <span class="item-title">Cuentas por pagar</span>
                                <i class="chevron fa fa-chevron-down" aria-hidden="true"></i>
                            </span>
                        </a>
                        <ul class="sidebar-submenu">
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Perfiles</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Roles</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-item">
                        <a title="Ventas">
                            <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                            <span>
                                <span class="item-title">Ventas</span>
                                <i class="chevron fa fa-chevron-down" aria-hidden="true"></i>
                            </span>
                        </a>
                        <ul class="sidebar-submenu">
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Perfiles</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Roles</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-item">
                        <a title="Seguridad">
                             <i class="fa fa-key" aria-hidden="true"></i>
                            <span>
                                <span class="item-title">Seguridad</span>
                                <i class="chevron fa fa-chevron-down" aria-hidden="true"></i>
                            </span>
                        </a>
                        <ul class="sidebar-submenu">
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Perfiles</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Roles</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-item">
                        <a title="Reportes">
                            <i class="fa fa-file-text" aria-hidden="true"></i>
                            <span>
                                <span class="item-title">Reportes</span>
                                <i class="chevron fa fa-chevron-down" aria-hidden="true"></i>
                            </span>
                        </a>
                        <ul class="sidebar-submenu">
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Perfiles</span>
                                    </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a title="Usuarios">
                                    <span>
                                        <span class="item-title">Roles</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </nav>
        </div>
    </div>
    <div id="layout" style="width:100%">
        <div class="card">
            <div class="card-header">
                <div class="card-info">
                    <i class="fa fa-id-card-o" aria-hidden="true"></i>
                    <span class="card-title">Monedas</span>
                </div>
                <div class="card-actions">
                    <i class="fa fa-search"></i>
                </div>
            </div>
            <div class="card-body">
                <div class="form-container">
                    <div class="input-wrapper">
                        <label>Codigo:</label>
                        <input>
                    </div>
                    <div class="input-wrapper">
                        <label>Nombre:</label>
                        <input>
                        <label>Predeterminada</label>
                        <input type="checkbox">
                    </div>
                    <div class="input-wrapper">
                        <label>Simbolo de la moneda:</label>
                        <input>
                    </div>
                    <div class="input-wrapper">
                        <label>Tasa de cambio para facturas de ventas:</label>
                        <input>
                    </div>
                    <div class="input-wrapper">
                        <label>Cambiar la tasa de cambio cada:</label>
                        <input>
                        <span>dias</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>