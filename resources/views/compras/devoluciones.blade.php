@extends('layouts.layouts')
@section('content')
<div class="custom-card full">
    <div class="custom-card-header">
        <div class="custom-card-info"> <i class="fa fa-id-card-o" aria-hidden="true"></i> <span class="custom-card-title">Documento de devolucion</span> </div>
        <div class="custom-card-actions"> <i class="fa fa-search"></i> </div>
    </div>
    <div class="custom-card-body">
		<div class="full-width padding">
			<!-- Card: contenedor principal -->
			<div class="card-main-wrapper only-one">
				<div class="card-header">
					<span class="card-title">Carga de documento</span>
				</div>
				<div class="custom-slider-wrapper">
					<div id="first-step" class="slide">
						<div class="vertical-list-input">
							<ul>
								<li><span class="step-text">Paso 1:</span></li>
								<li><span class="only-one-heading">Seleccione el tipo de documento a devolver</span></li>
								<li>
									<select class="custom-select big">
										<option><label>Factura de compras</label></option>
										<option><label>Notas de entrega</label></option>
									</select>
								</li>
								<li>
									<button class="custom-btn next">Siguiente</button>
								</li>
							</ul>
						</div>
					</div>
					<div id="second-step" class="slide">
						<div class="vertical-list-input">
							<ul>
								<li><span class="step-text">Paso 2:</span></li>
								<li><span class="only-one-heading">Cargue el documento</span></li>
								<li><input type="file"></li>
								<li>
									<button class="custom-btn next">Siguiente</button>
									<button class="custom-btn back">Volver</button>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!--
			<div class="card-main-wrapper">
				<div class="card-header">
					<span class="card-title">Pendientes</span>
				</div>
				<div class="vertical-list-input">
					<ul>
						<li>
							<span>Orden de compra</span>
						</li>
						<li>
							<span>Cambios de compra</span>
						</li>
					</ul>
				</div>
			</div>
			-->
			<!--
				<div class="table-main-wrapper">
					<div class="table-header">
						<span class="table-description">Ùltimas Transacciones</span>
					</div>
					<div class="datatable-wrapper custom">
						<table id="orden-compra-datatable" class="datatable display compact" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Tipo</th>
									<th>Documento</th>
									<th>Fecha</th>
									<th>Monto</th>
									<th>Estatus</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>ODC</td>
									<td>00000001</td>
									<td>05/03/2018</td>
									<td>0,00</td>
									<td>Pendiente por recibir</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				-->
		</div>
    </div>
</div>
@stop
@section('scripts')
	<script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="/libs/DataTables/datatables.min.js"></script>
	<script type="text/javascript" src="/js/global.js"></script>
	<script type="text/javascript" src="/js/devoluciones.js"></script>
@stop