@extends('layouts.layouts')
@section('content')
<div class="custom-card">
    <div class="custom-card-header">
        <div class="custom-card-info"> <i class="fa fa-id-card-o" aria-hidden="true"></i> <span class="custom-card-title">Orden de Compra</span> </div>
        <div class="custom-card-actions">
			<div>
				<a>Ordenar</a>
				<ul>
					<li>segun reposicion de inventario</li>
					<li>contra pedidos</li>
					<li>contra presupuestos</li>
					<li>ordenes manuales</li>
				</ul>
			</div>
			<i class="fa fa-search"></i> 
		
		</div>
    </div>
    <div class="custom-card-body">
        <div class="form-container">
			<div class="">
				<!--
				<div class="">
					<label>Proveedor</label>
					<input>
				</div>
-->
				<div class="">
					<span>Euro Perfumes C.A</span>
					<span><i class="fa fa-info"></i></span>
				</div>
				<div class="two-col">
					<div class="left-box-container">
						<div class="card-main-wrapper">
							<div class="card-header">
								<span class="card-title">Situacion Actual</span>
							</div>
							<div class="vertical-list-input">
								<ul>
									<li>
										<span>Saldo Actual</span>
										<span class="value">100</span>
									</li>
									<li>
										<span>Dias de vencida</span>
										<span class="value">10</span>
									</li>
									<li>
										<span>Credito disponible</span>
										<span class="value">100000</span>
									</li>
									<li>
										<span>Dias de credito</span>
										<span class="value">5</span>
									</li>
								</ul>
							</div>
						</div>
						<div class="card-main-wrapper">
							<div class="card-header">
								<span class="card-title">Pendientes</span>
							</div>
							<div class="vertical-list-input">
								<ul>
									<li>
										<span>Orden de compra</span>
									</li>
									<li>
										<span>Cambios de compra</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="table-main-wrapper">
						<div class="table-header">
							<span class="table-description">Ùltimas Transacciones</span>
						</div>
						<div class="datatable-wrapper custom">
							<table id="orden-compra-datatable" class="datatable display compact" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Tipo</th>
										<th>Documento</th>
										<th>Fecha</th>
										<th>Monto</th>
										<th>Estatus</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>ODC</td>
										<td>00000001</td>
										<td>05/03/2018</td>
										<td>0,00</td>
										<td>Pendiente por recibir</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
            <div class="Botonera">
                <button class="buttonGreen"><i class="fa fa-check-circle-o" aria-hidden="true"></i>Aceptar</button>
                <button class="buttonRed"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</button>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
	<script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="/libs/DataTables/datatables.min.js"></script>
	<script type="text/javascript" src="/js/global.js"></script>
	<script type="text/javascript" src="/js/orden-compra.js"></script>
@stop