@extends('layouts.layouts')
@section('content')
<div class="custom-card cardSmall">
    <div class="custom-card-header">
        <div class="custom-card-info">
            <i class="fa fa-id-card-o" aria-hidden="true"></i>
            <span class="custom-card-title">Gestionar</span>
        </div>
        <div class="custom-card-actions">
            <!--<i class="fa fa-search"></i>-->
            <!--<input type="text" id="search" placeholder="ID">-->
        </div>
    </div>
    <div class="custom-card-body">
        <div class="form-container">                            
            <table id="formulario">
                    <tr>
                        <th colspan="2">Datos del Usuario:</th>
                    </tr>
                    <tr>
                        <td><label>Nombre:</label><input></td>
                        <td><label>Apellidos:</label><input></td>
                    </tr>
                    <tr>
                        <th colspan="2">Datos de Contacto:</th>
                    </tr>
                    <tr>
                        <td><label>Telefono:</label><input></td>
                        <td><label>Correo Electronico:</label><input></td>
                    </tr>
                    <tr>
                        <th colspan="2">Dirección:</th>
                    </tr>
                    <tr>
                        <td>
                            <label>Estado:</label>
                            <select name="">
                                <option value="1">1</option> 
                                <option value="2">2</option> 
                                <option value="3">3</option> 
                                <option value="4">4</option> 
                            </select>
                        </td>
                        <td>
                            <label>Municipio:</label>
                            <select name="">
                                <option value="1">1</option> 
                                <option value="2">2</option> 
                                <option value="3">3</option> 
                                <option value="4">4</option> 
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Parroquia:</label>
                            <select name="">
                                <option value="1">1</option> 
                                <option value="2">2</option> 
                                <option value="3">3</option> 
                                <option value="4">4</option> 
                            </select>
                        </td>
                        <td>
                            <label>Detalle dirección:</label>
                            <input>
                        </td>
                    </tr>

                    <tr>
                        <th colspan="2">Datos Organizacionales:</th>
                    </tr>
                    <tr>
                        <td>
                            <label>Oficina:</label>
                            <select name="">
                                <option value="1">1</option> 
                                <option value="2">2</option> 
                                <option value="3">3</option> 
                                <option value="4">4</option> 
                            </select>
                        </td>
                        <td>
                            <label>Cargo:</label><input disabled>
                        </td>
                    </tr>
                </table>
            <div class="Botonera">
                <button class="buttonGreen"><i class="fa fa-check-circle-o" aria-hidden="true"></i>Aceptar</button>
                <button class="buttonRed"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</button>
            </div>

        </div>
    </div>
</div>
@stop