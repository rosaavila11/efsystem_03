@extends('layouts.layouts')
@section('content')
<div class="custom-card cardSmall">
    <div class="custom-card-header">
        <div class="custom-card-info">
            <i class="fa fa-id-card-o" aria-hidden="true"></i>
            <span class="custom-card-title">Roles</span>
        </div>
        <div class="custom-card-actions">
            <!--<i class="fa fa-search"></i>-->
            <!--<input type="text" id="search" placeholder="ID">-->
        </div>
    </div>
    <div class="custom-card-body">
        <div class="form-container">    
        <table class="" id="rolesForm">
            <tr>
                <th class="" colspan="2">Roles:</th>
            </tr>
            <tr>
                <td colspan="2">
                    <label>Seleccion:</label>
                    <select name="">
                            <option value="#">Rol N#</option> 
                            <option value="#">Rol N#</option>
                            <option value="#">Rol N#</option>
                            <option value="#">Rol N#</option>
                            <option value="#">Rol N#</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th colspan="2">Perfiles disponibles:</th>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="listaPerfiles">
                        <div class="checkbox">
                            <label><input type="checkbox" value="">Perfil 1</label><br>                                                    
                            <label><input type="checkbox" value="">Perfil 2</label><br>
                            <label><input type="checkbox" value="">Perfil 3</label><br>   
                            <label><input type="checkbox" value="">Perfil N</label><br> 
                            <label><input type="checkbox" value="">Perfil N</label><br> 
                            <label><input type="checkbox" value="">Perfil N</label><br> 
                            <label><input type="checkbox" value="">Perfil N</label><br>  
                            <label><input type="checkbox" value="">Perfil 3</label><br>   
                            <label><input type="checkbox" value="">Perfil N</label><br> 
                            <label><input type="checkbox" value="">Perfil N</label><br> 
                            <label><input type="checkbox" value="">Perfil N</label><br> 
                            <label><input type="checkbox" value="">Perfil N</label><br>  
                            <label><input type="checkbox" value="">Perfil 3</label><br>   
                            <label><input type="checkbox" value="">Perfil N</label><br> 
                            <label><input type="checkbox" value="">Perfil N</label><br> 
                            <label><input type="checkbox" value="">Perfil N</label><br> 
                            <label><input type="checkbox" value="">Perfil N</label><br>                                                
                        </div>
                    </div>
                </td>
            </tr>                                
        </table>                        
            <div class="Botonera">
                <button class="buttonGreen"><i class="fa fa-check-circle-o" aria-hidden="true"></i>Aceptar</button>
                <button class="buttonRed"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</button>
            </div>
        </div>
    </div>
</div>
@stop