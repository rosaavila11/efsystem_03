@extends('layouts.layouts')
@section('content')
<div class="custom-card cardMedium">
    <div class="custom-card-header">
        <div class="custom-card-info">
            <i class="fa fa-id-card-o" aria-hidden="true"></i>
            <span class="custom-card-title">Roles</span>
        </div>
        <div class="custom-card-actions">
            <!--<i class="fa fa-search"></i>-->
            <!--<input type="text" id="search" placeholder="ID">-->
        </div>
    </div>
    <div class="custom-card-body">
        <div class="form-container">    
        <table id="perfilesForm">
        <tr>
            <th colspan="3">Perfiles:</th>
        </tr>
        <tr>
            <td colspan="3">
                <label>Seleccion:</label>
                <select name="">
                        <option value="#">Perfil N#</option> 
                        <option value="#">Perfil N#</option>
                        <option value="#">Perfil N#</option>
                        <option value="#">Perfil N#</option>
                        <option value="#">Perfil N#</option>
                </select>
            </td>
        </tr>
        <tr>                                                
            <td class="panel">
                <label>Modulo N:</label>
                <form>
                    <div class="checkbox">
                        <label><input type="checkbox" value="">Crear</label>                                        
                        <label><input type="checkbox" value="">Ver</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" value="">Editar</label>                                                        
                        <label><input type="checkbox" value="">Eliminar</label>
                    </div>
                </form>
            </td>
            <td class="panel">
                <label>Modulo N:</label>
                <form>
                    <div class="checkbox">
                        <label><input type="checkbox" value="">Crear</label>                                        
                        <label><input type="checkbox" value="">Ver</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" value="">Editar</label>                                                        
                        <label><input type="checkbox" value="">Eliminar</label>
                    </div>
                </form>
            </td>
            <td class="panel">
                <label>Modulo N:</label>
                <form>
                    <div class="checkbox">
                        <label><input type="checkbox" value="">Crear</label>                                        
                        <label><input type="checkbox" value="">Ver</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" value="">Editar</label>                                                        
                        <label><input type="checkbox" value="">Eliminar</label>
                    </div>
                </form>
            </td>                                                
        </tr>
        <tr>                                                
                <td class="panel">
                    <label>Modulo N:</label>
                    <form>
                        <div class="checkbox">
                            <label><input type="checkbox" value="">Crear</label>                                        
                            <label><input type="checkbox" value="">Ver</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox" value="">Editar</label>                                                        
                            <label><input type="checkbox" value="">Eliminar</label>
                        </div>
                    </form>
                </td>
                <td class="panel">
                    <label>Modulo N:</label>
                    <form>
                        <div class="checkbox">
                            <label><input type="checkbox" value="">Crear</label>                                        
                            <label><input type="checkbox" value="">Ver</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox" value="">Editar</label>                                                        
                            <label><input type="checkbox" value="">Eliminar</label>
                        </div>
                    </form>
                </td>
                <td class="panel">
                    <label>Modulo N:</label>
                    <form>
                        <div class="checkbox">
                            <label><input type="checkbox" value="">Crear</label>                                        
                            <label><input type="checkbox" value="">Ver</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox" value="">Editar</label>                                                        
                            <label><input type="checkbox" value="">Eliminar</label>
                        </div>
                    </form>
                </td>                                                
            </tr>
            <tr>                                                
                    <td class="panel">
                        <label>Modulo N:</label>
                        <form>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Crear</label>                                        
                                <label><input type="checkbox" value="">Ver</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Editar</label>                                                        
                                <label><input type="checkbox" value="">Eliminar</label>
                            </div>
                        </form>
                    </td>
                    <td class="panel">
                        <label>Modulo N:</label>
                        <form>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Crear</label>                                        
                                <label><input type="checkbox" value="">Ver</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Editar</label>                                                        
                                <label><input type="checkbox" value="">Eliminar</label>
                            </div>
                        </form>
                    </td>
                    <td class="panel">
                        <label>Modulo N:</label>
                        <form>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Crear</label>                                        
                                <label><input type="checkbox" value="">Ver</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" value="">Editar</label>                                                        
                                <label><input type="checkbox" value="">Eliminar</label>
                            </div>
                        </form>
                    </td>                                                
                </tr>                       
            </table>             
            <div class="Botonera">
                <button class="buttonGreen"><i class="fa fa-check-circle-o" aria-hidden="true"></i>Aceptar</button>
                <button class="buttonRed"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</button>
            </div>
        </div>
    </div>
</div>
@stop