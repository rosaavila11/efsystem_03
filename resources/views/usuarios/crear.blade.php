@extends('layouts.layouts')
@section('content')

<div class="custom-card cardSmall">
    <div class="custom-card-header">
        <div class="custom-card-info"> <i class="fa fa-id-card-o" aria-hidden="true"></i> <span class="custom-card-title">Crear</span> </div>
        <!--<div class="custom-card-actions"> <i class="fa fa-search"></i> </div>-->
    </div>
     <form id="f_nuevo_usuario" action="agregar_nuevo_usuario" method="POST">

                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <div class="custom-card-body">
        <div class="form-container">
            <table id="formulario">
                <tr>
                    <th colspan="2">Datos de la Cuenta:</th>
                </tr>
                <tr>
                    <td colspan="2">
                        <label>Login:</label>
                        <input type="text" name="email" >
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Nombre:</label>
                        <input type="text" name="name">
                    </td>
                    <td>
                        <label>Apellidos:</label>
                        <input type="text" name="apellido">
                    </td>
                </tr>
                
                
                <tr>
                    <td colspan="2">
                        <label>Rol:</label>
                        <select name="">
                            <option value="1">Rol 1</option>
                            <option value="2">Rol 2</option>
                            <option value="3">Rol 3</option>
                            <option value="4">Rol 4</option>
                            <option value="3">Rol 5</option>
                            <option value="4">Rol 6</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <label>Cargo:</label>
                        <select name="">
                            <option value="1">Rol 1</option>
                            <option value="2">Rol 2</option>
                            <option value="3">Rol 3</option>
                            <option value="4">Rol 4</option>
                            <option value="3">Rol 5</option>
                            <option value="4">Rol 6</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Clave:</label>
                        <input type="password" name="password">
                    </td>
                    <td>
                        <label>Repetir clave:</label>
                        <input type="password" name="password">
                    </td>
                </tr>
            </table>
            <div class="Botonera">
                <button class="buttonGreen"><i class="fa fa-check-circle-o" aria-hidden="true"></i>Aceptar</button>
                <button class="buttonRed"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</button>
            </div>
        </div>
    </div>
</div>
</form>
@stop
@section('scripts')
    <script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="/libs/DataTables/datatables.min.js"></script>
    <script type="text/javascript" src="/js/global.js"></script>
    <script type="text/javascript" src="/js/orden-compra.js"></script>
@stop