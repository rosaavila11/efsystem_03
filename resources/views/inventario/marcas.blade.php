@extends('layouts.layouts')
@section('content')
<div class="custom-card cardSmall">
    <div class="custom-card-header">
        <div class="custom-card-info"> <i class="fa fa-file-text-o" aria-hidden="true"></i> <span class="custom-card-title">Marcas</span> </div>
        <div class="custom-card-actions"> <i class="fa fa-search"></i> </div>
    </div>
    <div class="custom-card-body">
        <div class="form-container inventario">
			<div class="card-body-small">                                            
				<form action="" class="smallView">
					<div class="flex-container">
						<div><label>Código:</label><input><i class="fa fa-search" aria-hidden="true"></i></div>
						<div><label>Nombre:</label><input></div>                                                            
					</div> 
				</form>                                                                                                
				<div class="Botonera">
					<button class="buttonGreen"><i class="fa fa-check-circle-o" aria-hidden="true"></i>Aceptar</button>
					<button class="buttonRed"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</button>
				</div>   
			</div>
        </div>
    </div>
</div>
@stop
@section('scripts')
	<!--<script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="/js/global.js"></script>-->
@stop
