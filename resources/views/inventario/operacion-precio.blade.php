@extends('layouts.layouts')
@section('content')
<div class="custom-card cardMedium">
    <div class="custom-card-header">
        <div class="custom-card-info"> <i class="fa fa-file-text-o" aria-hidden="true"></i> <span class="custom-card-title">Operaciones de Precio</span> </div>
        <!--<div class="custom-card-actions"> <i class="fa fa-search"></i> </div>-->
    </div>
    <div class="custom-card-body">
    <style>
        body {font-family: Arial;}

        /* Style the tab */
        .tab {
            //overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
            width: 100px;
            display: block;
        }

        .tab:hover {
            width: 100%;
        }

        /* Style the buttons inside the tab */
        .tab button {
            /*background-color: inherit;
            float: left;*/
            /*outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            font-size: 17px;*/
            border: none;
            width: 100%;
            padding: 2px 10px;
            margin: 0;
            border-radius: 0;
            background: #586e95;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;
        }

        /* Create an active/current tablink class */
        .tab button.active {
            background-color: #262e40;
        }

        /* Style the tab content */
        .tabcontent {
            display: none;
            //padding: 6px 12px;
            //border: 1px solid #ccc;
            //border-top: none;
            width: 100%;
        }
        .desc{
            margin: 1px;
            width:100%;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th{
            background-color: #586e95;
            color: white;
        }

        th, td {
            //padding: 8px;
            text-align: Center;
            border-bottom: 1px solid #ddd;
            font-size: 14px;
            width:20%
        }

        .content-promo{
            overflow: auto;
            height: 200px;
        }
        </style>
        </head>
        <body>

        <div class="tab">
        <button class="tablinks" onclick="openCity(event, 'London')">Ofertas y Promociones</button>
        <button class="tablinks" onclick="openCity(event, 'Paris')">Ajuste de precios e impuestos</button>
        <button class="tablinks" onclick="openCity(event, 'Tokyo')">Precio por cantidad de Productos</button>
        <button class="tablinks" onclick="openCity(event, 'Tokyo')">Listas de Precios</button>
        </div>

<div class="desc">
        <div id="London" class="tabcontent">
            <div class="content-promo">
                <table>
                    <tr>
                        <th>Nombre de la <br> Promoción</th>
                        <th>Fecha de <br> Inicio</th>
                        <th>Fecha de <br> Finalización</th>
                    </tr>
                    <tr>
                        <td contenteditable='true'>Peter</td>
                        <td>07/03/2018</td>
                        <td>07/03/2018</td>
                    </tr>
                    <tr>
                        <td contenteditable='true'>Lois</td>
                        <td>07/03/2018</td>
                        <td>07/03/2018</td>
                    </tr>
                    <tr>
                        <td contenteditable='true'>Joe</td>
                        <td>07/03/2018</td>
                        <td>07/03/2018</td>
                    </tr>
                    <tr>
                        <td contenteditable='true'>Joe</td>
                        <td>07/03/2018</td>
                        <td>07/03/2018</td>
                    </tr>
                    <tr>
                        <td contenteditable='true'>Joe</td>
                        <td>07/03/2018</td>
                        <td>07/03/2018</td>
                    </tr>
                    <tr>
                        <td contenteditable='true'>Joe</td>
                        <td>07/03/2018</td>
                        <td>07/03/2018</td>
                    </tr>
                    <tr>
                        <td contenteditable='true'>Joe</td>
                        <td>07/03/2018</td>
                        <td>07/03/2018</td>
                    </tr>
                    <tr>
                        <td contenteditable='true'>Joe</td>
                        <td>07/03/2018</td>
                        <td>07/03/2018</td>
                    </tr>
                    <tr>
                        <td contenteditable='true'>Joe</td>
                        <td>07/03/2018</td>
                        <td>07/03/2018</td>
                    </tr>
                    <tr>
                        <td contenteditable='true'>Joe</td>
                        <td>07/03/2018</td>
                        <td>07/03/2018</td>
                    </tr>
                    <tr>
                        <td contenteditable='true'>Joe</td>
                        <td>07/03/2018</td>
                        <td>07/03/2018</td>
                    </tr>

                </table>
            </div>
            <div class="Botonera">
				<button class="buttonGreen"><i class="fa fa-check-circle-o" aria-hidden="true"></i>Aceptar</button>
				<button class="buttonYellow" id="myBtn"><i class="fa fa-check-circle-o" aria-hidden="true"></i>Editar</button>
                <button class="buttonRed"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</button>
			</div>
        </div>
        

        <div id="Paris" class="tabcontent">
        <h3>2</h3>
        <p>Paris is the capital of France.</p> 
        </div>

        <div id="Tokyo" class="tabcontent">
        <h3>3</h3>
        <p>Tokyo is the capital of Japan.</p>
        </div>
</div>

        <script>
            function openCity(evt, cityName) {
                var i, tabcontent, tablinks;
            
                tabcontent = document.getElementsByClassName("tabcontent");
                for (i = 0; i < tabcontent.length; i++) {
                    tabcontent[i].style.display = "none";
                }
                tablinks = document.getElementsByClassName("tablinks");
                for (i = 0; i < tablinks.length; i++) {
                    tablinks[i].className = tablinks[i].className.replace(" active", ""); 
                }
                document.getElementById(cityName).style.display = "block";
                evt.currentTarget.className += " active";
            }
        </script>
    </div>
</div>
<!--Modal -->
<div id="myModal" class="modal" style="display: block">

  <!-- Modal content -->
  <div class="modal-content">
        <div class="card">
        <div class="card-header">
            <div class="card-info">
                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                <span class="card-title">Tipo de Promoción</span>
            </div>
        </div>
        <div class="card-body-modal">
                <div class="flex-container">
                
                        <div id="promoCant">
                            <input type="checkbox" class="tipoPromocion" id="promoCantidad" onclick="cantidadFunction()"><label>Por Cantidad</label>
                            <label>Lleve:</label><input id="lleve">
                            <label>Pague:</label><input id="pague">
                            <input type="checkbox" class="tipoPromocion" id="promoDescuento" onclick="descuentoFunction()"><label>Por Descuento</label><input id="descPromo"><br>
                            <input type="checkbox" class="tipoPromocion" id="promoPrecio" onclick="precioFunction()"><label>Por precio</label><input id="descPrecio">
                        </div>
                        <style>
                        #descPromo, #descPrecio {
                           // width: 100px;
                        }
                        #promoCant{
                            width: 300px;
                        }
                            #lleve, #pague{
                                width: 30px;
                            }
                            .tipoPromocion{
                                width: 20px !important;
                                
                            } 
                            .table-modal{
                                width: 100% !important;
                            }
                        </style>
                        <script>
                            function cantidadFunction() {
                                document.getElementById("porDescuento").style.display = "none";
                                document.getElementById("porPrecio").style.display = "none";
                                document.getElementById("porCantidad").style.display = "inline-table";
                                document.getElementById("porCantidad").style.width = "100%";
                                document.getElementById("promoDescuento").checked = false;
                                document.getElementById("promoPrecio").checked = false;
                            }
                            function precioFunction() {
                                document.getElementById("porDescuento").style.display = "none";
                                document.getElementById("porCantidad").style.display = "none";
                                document.getElementById("porPrecio").style.display = "inline-table";
                                document.getElementById("porPrecio").style.width = "100%";
                                document.getElementById("promoDescuento").checked = false;
                                document.getElementById("promoCantidad").checked = false;
                            }
                            function descuentoFunction() {
                                document.getElementById("porDescuento").style.display = "inline-table";
                                document.getElementById("porDescuento").style.width = "100%";
                                document.getElementById("porCantidad").style.display = "none";
                                document.getElementById("porPrecio").style.display = "none";
                                document.getElementById("promoPrecio").checked = false;
                                document.getElementById("promoCantidad").checked = false;
                            }
                        </script>
                        <div class="table-modal">
                            <table id="porCantidad">
                                <tr>
                                    <th>Cod. Producto:</th>
                                    <th>Nombre del Producto:</th>
                                    <th>Tipo:</th>
                                    <th>Lleve:</th>
                                    <th>Pague:</th>                                      
                                </tr>
                                <tr>
                                    <td>Cantidad</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>0,00</td>
                                    <td>0,00</td>
                                </tr><tr>
                                    <td>Cantidad</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>0,00</td>
                                    <td>0,00</td>
                                </tr><tr>
                                    <td>Cantidad</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>0,00</td>
                                    <td>0,00</td>
                                </tr><tr>
                                    <td>Cantidad</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>0,00</td>
                                    <td>0,00</td>
                                </tr><tr>
                                    <td>Cantidad</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>0,00</td>
                                    <td>0,00</td>
                                </tr><tr>
                                    <td>Cantidad</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>0,00</td>
                                    <td>0,00</td>
                                </tr><tr>
                                    <td>Cantidad</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>0,00</td>
                                    <td>0,00</td>
                                </tr><tr>
                                    <td>Cantidad</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>0,00</td>
                                    <td>0,00</td>
                                </tr>
                            </table>
                            <table id="porDescuento" style="display: none">
                                <tr>
                                    <th>Cod. Producto:</th>
                                    <th>Nombre del Producto:</th>
                                    <th>Tipo:</th>
                                    <th>% Descuento:</th>                                    
                                </tr>
                                <tr>
                                    <td>Descuento</td>
                                    <td>1</td>
                                    <td>0,00</td>
                                    <td>0,00</td>
                                </tr>
                            </table>
                            <table id="porPrecio" style="display: none">
                                <tr>
                                    <th>Cod. Producto:</th>
                                    <th>Nombre del Producto:</th>
                                    <th>Tipo:</th>
                                    <th>Tipo precio:</th>
                                    <th>Precio:</th>                                   
                                </tr>
                                <tr>
                                    <td>Precio</td>
                                    <td>1</td>
                                    <td>1</td>
                                    <td>0,00</td>
                                    <td>0,00</td>
                                </tr>
                            </table>
                        </div>                                                           
                        </div> 
                    <div class="Botonera">
                            <button class="buttonGreen"><i class="fa fa-check-circle-o" aria-hidden="true"></i>Aceptar</button>                          
                            <button class="buttonRed close"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</button>
                    </div>
        </div>
        </div>
  </div>
</div>  
@stop
@section('scripts')
	<!--<script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="/libs/DataTables/datatables.min.js"></script>
	<script type="text/javascript" src="/js/global.js"></script>-->
    <script type="text/javascript" src="/js/modal.js"></script>
@stop

