@extends('layouts.layouts')
@section('content')
<div class="custom-card">
    <div class="custom-card-header">
        <div class="custom-card-info"> <i class="fa fa-file-text-o" aria-hidden="true"></i> <span class="custom-card-title">Servicios</span> </div>
        <div class="custom-card-actions"> <i class="fa fa-search"></i> </div>
    </div>
    <div class="custom-card-body">
        <div class="form-container inventario">
		<form action="" class="inventario">
			<h2>Información General</h2>
			<div class="flex-container">
			<div><label>Código:</label><input><i class="fa fa-search" aria-hidden="true"></i></div>
			<div><label>Nombre:</label><input></div> 
			<div><label>Nombre Corto:</label><input></div>
			<div><label>Referencia:</label><input></div>
			<div><label>Servidor por defecto:</label><input></div>    
			<div><label>Cantidad Minima de Ventas:</label><input></div>
			<div><label>Días Garantía:</label><br>
				<select name="">
						<option value="#">N#</option> 
						<option value="#">N#</option>
						<option value="#">N#</option>
						<option value="#">N#</option>
						<option value="#">N#</option>
				</select>
			</div>
			</div>
				<h2>Comisiones y Configuración de Costos / Precios</h2>
				<div class="flex-container">
					<div><label>Comisión por Unidad Vendida:</label><input></div>
					<div><label>I.V.A. Venta:</label><input></div> 
					<div><label>% Comisión sobre Precio Unitario:</label><input></div>
					<div><label>Tipo de redondeo:</label><input></div>
					<div><label>I.V.A. Compra:</label><input></div>
					<div><label>Precio de Venta:</label><input></div>
				</div>
				<h2>Costos (Con Impuestos / Sin Impuestos)</h2>
				<div class="flex-container montos" >
					<div><label>Costo Proveedor:</label><br><input placeholder="0,00"><input placeholder="0,00"></div>
					<div><label>Costo Calculado:</label><br><input placeholder="0,00"><input placeholder="0,00"></div> 
					<div><label>Último Costo:</label><br><input placeholder="0,00"><input placeholder="0,00"></div>
					<div><label>Costo Promedio:</label><br><input placeholder="0,00"><input placeholder="0,00"></div>
				</div>
				<h2>Precios (Con Impuestos / Sin Impuestos)</h2>
				<div class="flex-container montos" >
					<div><label>Máximo:</label><br><input placeholder="0,00"><input placeholder="0,00"></div>
					<div><label>Oferta:</label><br><input placeholder="0,00"><input placeholder="0,00"></div> 
					<div><label>Mayor:</label><br><input placeholder="0,00"><input placeholder="0,00"></div>
					<div><label>Mínimo:</label><br><input placeholder="0,00"><input placeholder="0,00"></div>
				</div>
			</form>
			<div class="Botonera">
				<button class="buttonGreen"><i class="fa fa-check-circle-o" aria-hidden="true"></i>Aceptar</button>
				<button class="buttonRed"><i class="fa fa-times" aria-hidden="true"></i>Eliminar</button>
			</div>
        </div>
    </div>
</div>
@stop
@section('scripts')
	<!--<script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="/js/global.js"></script>-->
@stop
