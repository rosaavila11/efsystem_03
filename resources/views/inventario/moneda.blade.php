@extends('layouts.layouts')
@section('content')
<div class="custom-card cardSmall" id="draggable">
    <div class="custom-card-header">
        <div class="custom-card-info"> <i class="fa fa-file-text-o" aria-hidden="true"></i> <span class="custom-card-title">Monedas</span> </div>
        <div class="custom-card-actions"> <i class="fa fa-search"></i> </div>
    </div>
    <div class="custom-card-body">
		<form action="" class="moneda">
			<div class="flex-container">
				<div><label>Código:</label><input><i class="fa fa-search" aria-hidden="true"></i></div>
				<div><label>Nombre:</label><input></div>                                                            
			</div> 
			<h2>Detalle moneda</h2>
			<div class="flex-container">
				<div><label>Simbolo de la Moneda:</label><input></div>
				<div><label>Tasa de Cambio / Fac. Ventas:</label><input></div>
				<div><label>Cambiar tasa de cambio cada:</label>
					<select name="">
						<option value="#">N#</option> 
						<option value="#">N#</option>
						<option value="#">N#</option>
						<option value="#">N#</option>
						<option value="#">N#</option>
				</select> Días</div> 
				<div><label>Tasa de Cambio / Fac. Compra:</label><input></div>  
				<div><label>Notas:</label><input></div>       
			</div>
			<div class="Botonera">
				<button class="buttonGreen"><i class="fa fa-check-circle-o" aria-hidden="true"></i>Aceptar</button>
				<button class="buttonRed"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</button>
			</div>
		</form>     
	</div>
	
</div>
@stop
@section('scripts')
	
	
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#draggable" ).draggable();
  } );
  </script>

@stop
