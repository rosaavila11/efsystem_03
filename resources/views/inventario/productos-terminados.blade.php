@extends('layouts.layouts')
@section('content')
<div class="custom-card">
    <div class="custom-card-header">
        <div class="custom-card-info"> <i class="fa fa-file-text-o" aria-hidden="true"></i> <span class="custom-card-title">Productos Terminados</span> </div>
        <div class="custom-card-actions"> <i class="fa fa-search"></i> </div>
		
    </div>
    <div class="custom-card-body">
        <div class="form-container inventario">
			<div class="">
				<h2>Información General</h2>
				<div class="flex-container">
					<div><label>Código:</label><input><i id="myBtn" class="fa fa-search"></i></div>
					<div><label>Nombre:</label><input></div> 
					<div><label>Nombre Corto:</label><input></div>
					<div><label>Referencia:</label><input></div>

					<div><label>Departamento:</label><input></div>    
					<div><label>Marca:</label><input></div>
					<!--<div><label>Modelo:</label><input></div>-->
					<div><label>Días Garantía:</label><br>
						<select name="">
								<option value="#">N#</option> 
								<option value="#">N#</option>
								<option value="#">N#</option>
								<option value="#">N#</option>
								<option value="#">N#</option>
						</select>
					</div>
				</div>
				<h2>Configuración</h2>
				<div class="flex-container">
					<div><label>Unidad:</label><input></div>
					<div><label>I.V.A. Compra:</label><input></div> 
					<div><label>I.V.A. Venta:</label><input></div>
					<div><label>Precio Venta:</label><input></div>
				</div>
				<h2>Costos (Con Impuestos / Sin Impuestos)</h2>
				<div class="flex-container montos" >
					<div><label>Costo Proveedor:</label><br><input placeholder="0,00"><input placeholder="0,00"></div>
					<div><label>Costo Calculado:</label><br><input placeholder="0,00"><input placeholder="0,00"></div> 
					<div><label>Último Costo:</label><br><input placeholder="0,00"><input placeholder="0,00"></div>
					<div><label>Costo Promedio:</label><br><input placeholder="0,00"><input placeholder="0,00"></div>
				</div>
				<h2>Precios (Con Impuestos / Sin Impuestos)</h2>
				<div class="flex-container montos" >
					<div><label>Máximo:</label><br><input placeholder="0,00"><input placeholder="0,00"></div>
					<div><label>Oferta:</label><br><input placeholder="0,00"><input placeholder="0,00"></div> 
					<div><label>Mayor:</label><br><input placeholder="0,00"><input placeholder="0,00"></div>
					<div><label>Mínimo:</label><br><input placeholder="0,00"><input placeholder="0,00"></div>
				</div>
			</div>
            <div class="Botonera">
                <button class="buttonGreen"><i class="fa fa-check-circle-o" aria-hidden="true"></i>Aceptar</button>
                <button class="buttonRed"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- Para abrir modal -->
<!--<button id="myBtn">Open Modal</button>-->

<!--Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
        <div class="card">
        <div class="card-header">
            <div class="card-info">
                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                <span class="card-title">Productos Terminados</span>
            </div>
        </div>
        <div class="card-body-modal">
                <div class="flex-container">
                        <div>
                            <label>Nombre::</label><input><i class="fa fa-search" aria-hidden="true"></i>
                        </div>
                        <div>
                            <label>Código:</label><input><i class="fa fa-search" aria-hidden="true"></i>
                        </div>
                        <div>
                            <label>Referencia:</label><input><i class="fa fa-search" aria-hidden="true"></i>
                        </div> 
                        <div class="table-modal">
                                <table>
                                        <tr>
                                            <th>Codigo Producto:</th>
                                            <th>Nombre del Producto:</th>
                                            <th>Cantidad:</th>
                                            <th>Costo Calculado:</th>
                                            <th>Precio Máximo:</th>
                                            <th>Precio Oferta:</th>
                                            <th>Precio Mínimo:</th>
                                        </tr>
                                        <tr>
                                            <td>--</td>
                                            <td>--</td>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>1</td>
                                        </tr>
                                    </table>
                                </div>                                                           
                    </div> 
                    <div class="Botonera">
                            <button class="buttonGreen"><i class="fa fa-check-circle-o" aria-hidden="true"></i>Aceptar</button>
                            <button class="buttonRed close"><i class="fa fa-times" aria-hidden="true"></i>Cancelar</button>
                    </div>
        </div>
        </div>
  </div>
</div>   
@stop
@section('scripts')
	<!--<script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="/js/global.js"></script>-->
	<script type="text/javascript" src="/js/modal.js"></script>
@stop
