<div class="templatemo_footer">
      <div class="container">
      <div class="row">
        <div class="footer">
      <div class="container">
      <div class="row">
          <div class="col-md-9 col-sm-12">
            <br>
              <span><strong>© Copyleft 2018 | Banco Agrícola de Venezuela C.A, Banco Universal | RIF: G-20005795-5</strong></span>
            </div>
            <div class="col-md-3 col-sm-12 templatemo_rfooter">
                 <a href="https://www.facebook.com/BancoAgricolaVE">
                  <div class="hex_footer">
          <span class="fa fa-facebook"></span>
          </div>
                  </a>
                  <a href="https://twitter.com/bancoagricolave?lang=es">
                    <div class="hex_footer">
           <span class="fa fa-twitter"></span>
          </div>
                    </a>
                  <a href="https://www.instagram.com/bancoagricolave/">
                  <div class="hex_footer">
           <span class="fa fa-instagram"></span>
          </div>
                 </a>
                <a href="http://www.bav.com.ve/">
                  <div class="hex_footer">
           <span class="fa fa-desktop"></span>
          </div>
                </a>
            </div>
        </div>
        </div>
    </div>    <!-- footer end -->   
    <!-- footer end -->    