@extends('layouts.layouts')
@section('content')


<div class="box box-primary">
<div class="box-header">
                  <h3 class="box-title">Usuarios Encontrados</h3>
                </div>

<div class="box-body">              
<?php 

if( count($usuarios) >0){
?>

<table id="tabla_usuarios" class="display table table-hover" cellspacing="0" width="80%">
       
        <thead>
            <tr>
             <th style="width:40px">Id</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Teléfono</th>
                <th>Dirección</th>
                <th>email</th>
                <th>Fecha Creado</th>
                 <th>Acción</th>
            </tr>
        </thead>
 
        
       
<tbody>


<?php 

   foreach($usuarios as $usuario){  
?>

 <tr role="row" class="odd">
    <td class="sorting_1"><?= $usuario->id; ?></td>
    <td class="mailbox-messages mailbox-name" ><a href="javascript:void(0);" onclick="mostrarficha(<?= $usuario->id; ?>);"  style="display:block"><i class="fa fa-user"></i>&nbsp;&nbsp;<?= $usuario->name." ".$usuario->apellido;  ?></a></td>
    <td><?= $usuario->cedula;  ?></td>
    <td><?= $usuario->telefono;  ?></td>
    <td><?= $usuario->direccion;  ?></td>
    <td><?= $usuario->email ?></td>
    <td><?= $usuario->created_at;  ?></td>
    <td><button type="button" class="btn submit-btn" onclick="mostrarficha(<?= $usuario->id; ?>);"><i class="fa fa-fw fa-eye"></i>Editar</button></td>

  <!--  <td><button class="btn  btn-skin-green btn-xs" onclick="mostrarficha(<?= $usuario->id; ?>);" ><i class="fa fa-fw fa-eye"></i>Ver</button></td>-->
</tr>

<?php        
}
?>


  

    </table>



    <?php


echo str_replace('/?', '?', $usuarios->render() )  ;

}
else
{

?>


<br/><div class='rechazado'><label style='color:#FA206A'>...No se ha encontrado ningun usuario...</label>  </div> 

<?php
}

?>
</div>
@stop
@section('scripts')
    <script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="/libs/DataTables/datatables.min.js"></script>
    <script type="text/javascript" src="/js/global.js"></script>
    <script type="text/javascript" src="/js/orden-compra.js"></script>
@stop


