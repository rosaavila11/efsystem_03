@extends('layouts.layouts')


@section ('content')


 <div class="login-box">
      <div class="login-logo">
        <!--<a href="#"><b>Sistema</b>Laravel</a>-->
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Registro en el sistema</p>
       
        <form action="register" method="post">
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">   
          
          <div class="form-group has-feedback">
            {{ Form::label('name', 'Nombre completo') }}
            {{ Form::text('name', null, array('placeholder' => 'Introduce tu nombre y apellido', 'class' => 'form-control')) }}        
            <!--<input type="text" class="form-control" name="name" >-->
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>


           <div class="form-group has-feedback">
            {{ Form::label('email', 'Dirección de E-mail') }}
            {{ Form::text('email', null, array('placeholder' => 'Introduce tu E-mail', 'class' => 'form-control')) }}
             
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>

          <div class="form-group has-feedback">
            
      {{ Form::label('password', 'Contraseña') }}
      {{ Form::password('password', array('placeholder' => 'Contraseña', 'class' => 'form-control')) }}
      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    
  </div>
              
         
          <div class="row">
            

            
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Registrar</button>
            </div><!-- /.col -->
          </div>
        </form>

     
       


@endsection
