@extends('layouts.login')
@section ('content')
    <div id="container-main" class="row">
        <div class="half-row opacity-mask-1"> 
            <div class="marco">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <img class="d-block img-fluid" src="/img/carousel/1.jpg" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block img-fluid" src="/img/carousel/2.jpg" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block img-fluid" src=/img/carousel/3.jpg alt="Third slide">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="half-row">
            <div class="login-form-container row">
                <div class="login-form row">
                    <form action="login" method="post">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">   
                        <div id="login-img" class="row">
                            <h1 id="login-title"><img class="login-logo" src="/img/ef-logo.png" alt="ef"> <span>System</span></h1> 
						</div>
                         @include('messages')
                        <div class="login-input-wrapper row">
                            {{ Form::text('email', null, array('placeholder' => 'Introduce tu E-mail', 'class' => '')) }}
                            <i class="input-icon fa fa-user"></i> 
                        </div>
                        <div class="login-input-wrapper row">
            {{ Form::password('password', array('placeholder' => 'Introduce tu password', 'class' => '')) }}
                            <i class="input-icon fa fa-lock"></i>
                        </div>
                        <div class="login-input-wrapper submit-wrapper row">
                            <a href="">¿Recordar contraseña?</a>
                            <button class="btn submit-btn">Iniciar Sesion</button>
                        </div>
                    </form>
                    <div class="login-input-wrapper submit-wrapper row socialNetwork">
                        <a href="https://www.instagram.com/perfumesef/" title="Instagram" target="_blank">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                        <a href="https://www.facebook.com/perfumesefvzla" title="Facebook" target="_blank">
                            <i class="fa fa-facebook-square" aria-hidden="true"></i>
                        </a>
                        <a href="https://twitter.com/perfumesef" title="Twitter" target="_blank">
                            <i class="fa fa-twitter-square" aria-hidden="true"></i>
                        </a>
                        <a href="http://efperfumes.com/" title="Web Oficial" target="_blank">
                            <i class="fa fa-bandcamp" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="login-input-wrapper submit-wrapper row derechosReservados">
                        Todos los derechos Reservados. Versión 1.0
                    </div>
                </div>
            </div>
        </div>
    </div>
     <!-- jQuery first, then Tether, then Bootstrap JS. -->
     <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
@endsection

