        // Obtiene el modal
        var modal = document.getElementById('myModal');
    
        // Boton para el modal
        var btn = document.getElementById("myBtn");
    
        // Boton para cerrar modal
        var span = document.getElementsByClassName("buttonRed close")[0];
    
        // Funcion para abrir modal
        btn.onclick = function() {
            modal.style.display = "block";
        }
    
        // Funcion para el boton de cerrar
        span.onclick = function() {
            modal.style.display = "none";
        }
    
        // Para hacer click fuera del modal
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }