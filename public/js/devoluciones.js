$(document).ready(function () {
	$('.custom-btn.next').on('click', function () {
		$("#second-step").addClass('active');
	});
	$('.custom-btn.back').on('click', function () {
		$("#first-step").addClass('active');
		$("#second-step").removeClass('active');
	});
});