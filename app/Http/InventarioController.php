<?php 
namespace efsystem\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use efsystem\Http\Controllers\Controller;
use efsystem\User;
use Illuminate\Support\Facades\Auth;

class InventarioController extends Controller  {
    public function productosTerminados() {
        return view('inventario.productos-terminados');
    }
    public function productosCompuestos() {
        return view('inventario.productos-compuestos');
    }
    public function servicios() {
        return view('inventario.servicios');
    }
    public function unidades() {
        return view('inventario.unidades');
    }
    public function marcas() {
        return view('inventario.marcas');
    }
    public function operacionPrecio() {
        return view('inventario.operacion-precio');
    }
    public function departamentos() {
        return view('inventario.departamentos');
    }
    public function depositos() {
        return view('inventario.depositos');
    }
    public function moneda() {
        return view('inventario.moneda');
    }
}