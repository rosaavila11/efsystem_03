<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;


class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        //
      
        return view('login');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        //
        $this->validate($request, User::$login_validation_rules);
        $data = $request->only('email', 'password');
      
       if (\Auth::attempt($data)) {

             

            //return 'Is Logged in';
             //return view("home");
            return redirect()->intended('home');
            //return view('home');
           
        }

    $rules = [
        'Email' => 'required',
        'Password' => 'required',
        
    ];
 
    $this->validate($request, $rules);
        return view('login');              
        
                      
    }

        protected function getRegister()
    {
        return view("crear-usuario");
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $this->validate($request,User::$create_validation_rules);

        $data = $request->only('name','apellido','cedula','telefono','direccion',
        'email','password');
        //dd($data);die;
       $data ['password']=bcrypt($data['password']);
       $user= User::create($data);
       //dd($data);die;
       if ($user) {

           # code...
           \Auth::login($user);


            return redirect()->intended('login');
             ///return view ("se ha registrado correctamente el usuario");
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        //
        \Auth::logout();
        return redirect()->route('login');
    }
}
