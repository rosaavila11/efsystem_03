<?php 

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
  

class UsuariosController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	

	//presenta el formulario para nuevo usuario
	
public function store1(Request $request)
    {

      
        

        $data = $request->only('name','apellido','cedula','telefono','direccion',
        'email','password');

       $data ['password']=bcrypt($data['password']);
       $user= User::create($data);
       
       if ($user) {

         
		$usuarios= User::paginate(25);
        
        return view('listados.listado_usuarios')->with("usuarios", $usuarios );
       }
    }

public function form_editar_usuario($id)
	{
		//funcion para cargar los datos de cada usuario en la ficha
		$usuario=User::find($id);
		$contador=count($usuario);
		if($contador>0){          
            return view("formularios.form_editar_usuario")->with("usuario",$usuario);   
		}
		else
		{            
            return view("/home")->with("msj","El usuario Fue Creado");  
		}
	}



		public function editar_usuario()
	{

		$data=Request::all();
		$idUsuario=$data["id"];
		$usuario=User::find($idUsuario);

        $usuario->name  =  $data["name"];
		$usuario->apellido=$data["apellido"];
		$usuario->cedula=$data["cedula"];
		$usuario->telefono=$data["telefono"];
		$usuario->direccion=$data["direccion"];
		$usuario->email=$data["email"];

		
		$resul= $usuario->save();

		if($resul){
            
            return view("mensajes.msj_correcto")->with("msj","Datos actualizados Correctamente");   
		}
		else
		{
             
            return view("mensajes.msj_rechazado")->with("msj","hubo un error vuelva a intentarlo");  

		}
	}







}