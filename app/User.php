<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

      protected $table ="sys_usuario";

    protected $fillable = [
        'name','apellido','cedula','telefono','direccion','email','password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
   /* protected $hidden = [
        'password', 'remember_token',
    ];
*/
     public static $login_validation_rules =[

      'email' => 'required|email|exists:sys_usuario',
      'password' => 'required'
    ];

  

    public static $create_validation_rules =[

      'name' => 'required|unique:sys_usuario',

      'apellido' => 'required|unique:sys_usuario',

      'cedula' => 'required|unique:sys_usuario',

      'telefono' => 'required|unique:sys_usuario',

      'direccion' => 'required|unique:sys_usuario',

      'email' => 'required|email|unique:sys_usuario',

      'password' => 'required'
     
      
    ];
}
